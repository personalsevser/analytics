const proxy = require('http-proxy-middleware');

const apiProxyAuth = proxy('/api/auth/*', {
  target: 'http://localhost:4501',
  pathRewrite: {
    '/api/auth': '', // remove base path
  },
  changeOrigin: true,
});

const apiProxyGoods = proxy('/api/goods/*', {
  target: 'http://localhost:4502',
  pathRewrite: {
    '/api/goods': '', // remove base path
  },
  changeOrigin: true,
});

const apiProxyOrganizations = proxy('/api/organizations/*', {
  target: 'http://localhost:4503',
  pathRewrite: {
    '/api/organizations': '', // remove base path
  },
  changeOrigin: true,
});

module.exports = [apiProxyAuth, apiProxyGoods, apiProxyOrganizations];
