const express = require('express');
const cors = require('cors');
const config = require('./config');
const proxySettings = require('./proxy');

const app = express();

app.use(cors({
  origin(origin, callback) {
    if (config.whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  },
  credentials: true,
}));

proxySettings.forEach(item => app.use(item));

app.listen(config.port, (req, res) => {
  console.log(`Server is listening on port: ${config.port}`);
});
