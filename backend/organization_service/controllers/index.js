const Sequelize = require('sequelize');
const tokenHandler = require('../services/token');

const { ipEntity, ipFields } = require('../models/ip');
const { oooEntity, oooFields } = require('../models/ooo');
const { userToEntity } = require('../models/user_to_entity');


module.exports = function builder(app) {
  app.post('/create', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        let typeObject = null;
        const data = {};

        console.info(userInfo);

        if (req.body.type === 'ip') {
          typeObject = 'ip';
          ipFields.forEach((key) => {
            data[key] = req.body.value[key];
          });
        } else if (req.body.type === 'ooo') {
          typeObject = 'ooo';
          oooFields.forEach((key) => {
            data[key] = req.body.value[key];
          });
        } else {
          res.send(400, {
            message: 'Неверный набор параметров',
            success: false,
          });
        }

        data.owner_id = userInfo.id;

        console.log(data);
        console.log(typeObject);

        if (typeObject === 'ip') {
          ipEntity.create(data)
            .then((entity) => {
              const newUserToEntity = {
                idUser: userInfo.id,
                idEntity: entity.dataValues.id,
              };
              console.log(newUserToEntity);
              userToEntity.create(newUserToEntity)
                .then(() => {
                  res.send({
                    message: 'Запись успешно создана',
                    success: true,
                    result: entity,
                  });
                })
                .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
            })
            .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
        } else {
          oooEntity.create(data)
            .then((entity) => {
              userToEntity.create({
                idUser: userInfo.id,
                idEntity: entity.dataValues.id,
              })
                .then(() => {
                  res.send({
                    message: 'Запись успешно создана',
                    success: true,
                    result: entity,
                  });
                })
                .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
            })
            .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
        }
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.post('/filter', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        const where = {
          userId: userInfo.id,
        };
        userToEntity.findAll({
          where: {
            idUser: {
              [Sequelize.Op.eq]: userInfo.id,
            },
          },
        })
          .then((userToEntityFound) => {
            console.log(userToEntityFound);
            console.log(userToEntityFound[0].dataValues);
            console.log(userToEntityFound.map(({ dataValues }) => dataValues.idEntity));
            const secondWhere = {
              id: {
                [Sequelize.Op.in]: userToEntityFound.map(({ dataValues }) => dataValues.idEntity),
              },
            };
            console.log(secondWhere);
            Promise.all([
              ipEntity.findAll({
                where: secondWhere,
              }),
              oooEntity.findAll({
                where: secondWhere,
              }),
            ])
              .then((result) => {
                res.send({
                  message: 'success',
                  success: true,
                  result: {
                    ip: result[0],
                    ooo: result[1],
                  },
                });
              })
              .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
};
