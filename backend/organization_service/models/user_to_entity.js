const Sequelize = require('sequelize');
const db = require('../provider');

const fields = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  idUser: {
    type: Sequelize.INTEGER,
  },
  idEntity: {
    type: Sequelize.INTEGER,
  },
};

const userToEntity = db.define('user_to_entity', fields, { freezeTableName: true });

module.exports = {
  db,
  userToEntity,
};
