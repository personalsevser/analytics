const Sequelize = require('sequelize');
const db = require('../provider');

const fields = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING(45),
    allowNull: false,
  },
  inn: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  index: {
    type: Sequelize.INTEGER,
  },
  fname: {
    type: Sequelize.STRING(20),
    allowNull: false,
  },
  lname: {
    type: Sequelize.STRING(45),
    allowNull: false,
  },
  mname: {
    type: Sequelize.STRING(45),
  },
  regionCode: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  street: {
    type: Sequelize.STRING(45),
    allowNull: false,
  },
  building: {
    type: Sequelize.STRING(4),
    allowNull: false,
  },
  block: {
    type: Sequelize.STRING(3),
  },
  flat: {
    type: Sequelize.STRING(4),
    allowNull: false,
  },
  phoneNumber: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING(45),
    allowNull: false,
  },
  bankAccount: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  bankName: {
    type: Sequelize.STRING(45),
    allowNull: false,
  },
  bankCorAccount: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  RCBIC: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
};


const keyFields = Object.keys(fields);
keyFields.splice(0, 2);

const ipFields = ['name', 'inn', 'index', 'fname', 'lname', 'mname', 'regionCode', 'street', 'building', 'block', 'flat', 'phoneNumber', 'email', 'bankAccount', 'bankName', 'bankCorAccount', 'RCBIC'];

const ip = db.define('ipEntity', fields, {
  freezeTableName: true,
  charset: 'utf8',
  collate: 'utf8_general_ci',
});

module.exports = {
  db,
  ipEntity: ip,
  ipFields,
};
