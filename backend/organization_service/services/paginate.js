module.exports = (query, { page, pageSize }) => {
  const offset = page * pageSize;
  return {
    ...query,
    offset,
    limit: pageSize,
  };
};
