const xlsx = require('node-xlsx');
const utf8 = require('utf8');

function exportFromXSLXWb(buffer, fieldNames, fieldsWb) {
  const readedData = xlsx.parse(buffer);
  const fieldPattern = fieldsWb.reduce((accum, field) => {
    accum[field.fieldName] = fieldNames.indexOf(field.label);
    return accum;
  }, {});

  delete fieldPattern.price;

  let resultArray = JSON.parse(JSON.stringify(readedData[0].data));
  resultArray.splice(0, 3);
  resultArray = resultArray.filter((goodsData) => goodsData.length);
  resultArray = resultArray.map((goodsData) => {
    const prepareObject = {};
    Object.keys(fieldPattern).forEach((fieldName) => {
      prepareObject[fieldName] = goodsData[fieldPattern[fieldName]];
    });
    return prepareObject;
  });
  return resultArray;
}
function exportFromXSLX(buffer, fieldNames) {
  const readedData = xlsx.parse(buffer);
  const fieldPattern = {
    name: fieldNames.indexOf('name'),
    barcode: fieldNames.indexOf('barcode'),
    price: fieldNames.indexOf('price'),
    description: fieldNames.indexOf('description'),
    size: fieldNames.indexOf('size'),
  };

  let resultArray = JSON.parse(JSON.stringify(readedData[0].data));
  resultArray.splice(0, 1);
  resultArray = resultArray.map(goodsData => {
    const prepareObject = {};
    fieldNames.forEach(fieldName => {
      prepareObject[fieldName] = goodsData[fieldPattern[fieldName]];
    });
    prepareObject.price = prepareObject.price.toFixed(0);
    return prepareObject;
  });
  return resultArray;
}

function getFields(buffer) {
  const readedData = xlsx.parse(buffer);
  return readedData[0].data[0];
}

function getFieldsWb(buffer) {
  const readedData = xlsx.parse(buffer);
  return readedData[0].data[2];
}

module.exports = {
  exportFromXSLX,
  exportFromXSLXWb,
  getFields,
  getFieldsWb,
};
