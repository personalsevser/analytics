const jwt = require('jsonwebtoken');
const config = require('../config');

module.exports = function (token) {
  return new Promise((resolve, reject) => {
    if (!token) {
      reject(null);
    }
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }
    jwt.verify(token, config.accessTokenKey, (err, decoded) => {
      if (err) {
        reject(null);
      } else {
        resolve(decoded);
      }
    })
  });
};
