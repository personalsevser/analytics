const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const provider = require('./provider');
const controllers = require('./controllers');
const fileUpload = require('express-fileupload');


const app = express();

app.use(fileUpload());
app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));

provider
  .authenticate()
  .then((data) => {
    controllers(app);

    app.listen(config.port, (req, res) => {
      console.log(`Server is listening on port: ${config.port}`);
    });
  });
