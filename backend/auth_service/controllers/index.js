const jwt = require('jsonwebtoken');
const axios = require('axios');
const { Op } = require('sequelize');
const encrypt = require('../services/encryption');
const mail = require('../services/mail');
const db = require('../models/user');
const config = require('../config');

module.exports = function (app) {
  app.post('/getUserParams', (req, res) => {
    let token = req.headers.authorization; // Express headers are auto converted to lowercase
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }

    if (token) {
      jwt.verify(token, config.accessTokenKey, (err, decoded) => {
        if (err) {
          return res.json({
            success: false,
            message: 'Токен необходимо обновить',
            refreshToken: true,
          });
        }
        res.send(decoded);
      });
    } else {
      res.send(401, {
        message: 'Аккаунт не авторизован',
        success: false,
      });
    }
  });
  app.post('/refreshToken', (req, res) => {
    let token = req.headers.authorization;
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }

    if (token) {
      jwt.verify(token, config.refreshTokenKey, (err, decoded) => {
        if (err && !decoded.email) {
          res.send(401, {
            message: 'Аккаунт не авторизован',
            success: false,
          });
        }
        db.user
          .findOne({
            where: {
              email: decoded.email,
            },
          })
          .then((entity) => {
            if (entity) {
              const accessToken = jwt.sign({ email: entity.email, id: entity.id }, config.accessTokenKey, { expiresIn: `${60 * 60 * 24 * 30}s` });
              const refreshToken = jwt.sign({ email: entity.email, id: entity.id }, config.refreshTokenKey, { expiresIn: `${60 * 60 * 24 * 30}s` });
              let expiresIn = 0;
              jwt.verify(accessToken, config.accessTokenKey, (err, decoded) => {
                expiresIn = decoded.exp;
              });
              return res.send({
                success: true,
                message: 'Успех!',
                expiresIn,
                accessToken,
                refreshToken,
                user: {
                  login: entity.login,
                  email: entity.email,
                },
              });
            }
            return res.send({ message: 'Текущего пользователя не существует', success: false });
          }).catch((error) => res.send({ message: 'Аккаунт не верифицирован!', success: false, error }));
      });
    } else {
      res.send(401, {
        message: 'Аккаунт не авторизован',
        success: false,
      });
    }
  });
  app.options('/signup', (req, res) => {
    res.send();
  });
  app.options('/signin/:token', (req, res) => {
    res.send();
  });
  app.options('/verificateToken', (req, res) => {
    res.send();
  });
  app.post('/verificateToken', (req, res) => {
    db.user.findOne({
      where: {
        verificationToken: req.body.token,
      },
    })
      .then((entity) => {
        if (!entity || Array.isArray(entity)) {
          return res.send({ message: 'Ошибка токена верификации!', success: false });
        }

        const data = { verificationToken: null };

        // return res.redirect('/auth/signin');
        db.user.update(data, {
          where: {
            id: entity.id,
          },
        }).then(() => res.send({ message: 'Ваш аккаунт активирован!', success: true }))
          .catch((error) => res.send(401, {
            message: 'Аккаунт не требует верифицикации!',
            success: false,
            error,
          }));
      });
  });
  app.get('/signin/:token', (req, res) => {
    db.user.findOne({
      where: {
        verificationToken: req.params.token,
      },
    })
      .then((entity) => {
        if (!entity || Array.isArray(entity)) {
          return res.send({ message: 'Ошибка токена верификации!', success: false });
        }

        const data = { verificationToken: null };

        // return res.redirect('/auth/signin');
        db.user.update(data, {
          where: {
            id: entity.id,
          },
        }).then(() => res.send({ message: 'Ваш аккаунт активирован.', success: true }))
          .catch((error) => res.send(401, {
            message: 'Аккаунт не требует верифицикации!',
            success: false,
            error,
          }));
      });
  });
  app.post('/signup', (req, res) => {
    if (req.body.username && req.body.password && req.body.acceptAgreement && req.body.recaptchav2) {
      const secret_key = '6LeOpcYUAAAAAAyM8p5ek-SUHcbeEIBEH1jVbLuu';
      const token = req.body.recaptchav2;
      axios.post(`https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`)
        .then(({ data }) => {
          if (data.success) {
            db.user
              .findOne({
                where: {
                  email: req.body.username,
                },
              })
              .then((entity) => {
                if (entity) {
                  res.send(400, {
                    message: 'Пользователь с таким email существует',
                    success: false,
                  });
                } else {
                  const data = {
                    email: req.body.username.toLowerCase(),
                    password: encrypt.generateHash(req.body.password),
                    verificationToken: encrypt.generateToken(),
                  };

                  db.user.create(data)
                    .then(() => {
                      const resetLink = `${req.headers.origin}/Account/${data.verificationToken}`;
                      const mailText = `Привет!

Твоя ссылка для подтверждения регистрации: ${resetLink}`;
                      mail(req.body.username.toLowerCase(), 'Добро пожаловать в WB Analytics!', mailText);
                      res.send({ message: 'Пользователь успешно зарегистрирован', success: true });
                    })
                    .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
                }
              });
          } else {
            res.send(400, { message: 'Произошла ошибка, попробуйте повторить позже', success: false });
          }
        })
        .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
    } else {
      res.send(400, { message: 'Неверный набор параметров', success: false });
    }
  });

  app.post('/forgotPassword', (req, res) => {
    if (req.body.email) {
      db.user
        .findOne({
          where: {
            email: req.body.email,
          },
        })
        .then((entity) => {
          if (entity) {
            const data = {
              resetPasswordToken: encrypt.generateToken(),
              resetPasswordExpires: Date.now() + 60 * 60 * 1000,
            };

            db.user.update(data, {
              where: {
                id: entity.id,
              },
            }).then(() => {
              const resetLink = `${req.headers.origin}/Reset/${data.resetPasswordToken}`;
              const mailText = `Привет! 
              
Для восстановления аккаунта перейдите по ссылке:  ${resetLink}`;
              mail(req.body.email.toLowerCase(), 'Восстановление пароля для WB Analytics!', mailText);
              res.send({ message: 'Если Ваш логин зарегистрирован в нашем сервисе, то мы уже отправили Вам письмо!', success: true });
            })
              .catch(() => res.send({ message: 'Если Ваш логин зарегистрирован в нашем сервисе, то мы уже отправили Вам письмо!', success: true }));
          }
          res.send({ message: 'Если Ваш логин зарегистрирован в нашем сервисе, то мы уже отправили Вам письмо!', success: true })
        })
        .catch(() => res.send({ message: 'Если Ваш логин зарегистрирован в нашем сервисе, то мы уже отправили Вам письмо!', success: true }));
    } else {
      res.send(400, { message: 'Неверный набор параметров', success: false });
    }
  });
  app.post('/resetPassword', (req, res) => {
    if (req.body.resetPasswordToken && req.body.password) {
      db.user.findOne({
        where: {
          resetPasswordToken: req.body.resetPasswordToken,
          resetPasswordExpires: { [Op.gt]: Date.now() },
        },
      })
        .then((entity) => {
          if (!entity || Array.isArray(entity)) {
            return res.send({ message: 'Срок действия ссылки истек', success: false });
          }

          const data = {
            password: encrypt.generateHash(req.body.password),
            resetPasswordToken: null,
            resetPasswordExpires: null,
          };

          db.user.update(data, {
            where: {
              id: entity.id,
            },
          }).then(() => res.send({ message: 'Пароль изменен успешно!', success: true }))
            .catch(() => res.send(400, {
              message: 'Произошла ошибка восстановления пароля, повторите попытку позже',
              success: false,
            }));
        });
    }
  });

  app.post('/signin', (req, res) => {
    if (req.body.email && req.body.password && (req.body.recaptcha || req.body.recaptchav2)) {
      if (req.body.recaptcha) {
        const secret_key = '6LdyfMUUAAAAALKafDeHyWVIQxSB2Bk39cyRNvQk';
        const token = req.body.recaptcha;
        axios.post(`https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`)
          .then(({ data }) => {
            if (data.success) {
              db.user
                .findOne({
                  where: {
                    email: req.body.email,
                    verificationToken: null,
                  },
                }).then((entity) => {
                  if (entity) {
                    if (encrypt.isValidPassword(req.body.password, entity.password)) {
                      const accessToken = jwt.sign({ email: entity.email, id: entity.id }, config.accessTokenKey, { expiresIn: `${60 * 60 * 24 * 30}s` });
                      const refreshToken = jwt.sign({ email: entity.email, id: entity.id }, config.refreshTokenKey, { expiresIn: `${60 * 60 * 24 * 30}s` });
                      let expiresIn = 0;
                      jwt.verify(accessToken, config.accessTokenKey, (err, decoded) => {
                        expiresIn = decoded.exp;
                      });
                      return res.send({
                        success: true,
                        message: 'Успех!',
                        expiresIn,
                        accessToken,
                        refreshToken,
                        data,
                        user: {
                          login: entity.login,
                          email: entity.email,
                        },
                      });
                    }

                    return res.send(400, { message: 'Неверные Логин или Пароль!', success: false });
                  }
                  return res.send(400, { message: 'Неверные Логин или Пароль!', success: false });
                }).catch((error) => res.send({
                  message: 'Аккаунт не верифицирован!', success: false, error,
                }));
            } else {
              res.send(400, { message: 'Неверные Логин или Пароль!', success: false });
            }
          })
          .catch((error) => res.send({ message: 'Произошла ошибка авторизации', success: false, error }));
      } else {
        const secret_key = '6LeOpcYUAAAAAAyM8p5ek-SUHcbeEIBEH1jVbLuu';
        const token = req.body.recaptchav2;
        axios.post(`https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`)
          .then(({ data }) => {
            db.user
              .findOne({
                where: {
                  email: req.body.email,
                  verificationToken: null,
                },
              }).then((entity) => {
                if (entity) {
                  if (encrypt.isValidPassword(req.body.password, entity.password)) {
                    const accessToken = jwt.sign({ email: entity.email, id: entity.id }, config.accessTokenKey, { expiresIn: `${60 * 60 * 24 * 30}s` });
                    const refreshToken = jwt.sign({ email: entity.email, id: entity.id }, config.refreshTokenKey, { expiresIn: `${60 * 60 * 24 * 30}s` });
                    let expiresIn = 0;
                    jwt.verify(accessToken, config.accessTokenKey, (err, decoded) => {
                      expiresIn = decoded.exp;
                    });
                    return res.send({
                      success: true,
                      message: 'Успех!',
                      expiresIn,
                      accessToken,
                      refreshToken,
                      data,
                      user: {
                        login: entity.login,
                        email: entity.email,
                      },
                    });
                  }

                  return res.send(400, { message: 'Неверные Логин или Пароль!', success: false, data });
                }
                return res.send(400, { message: 'Неверные Логин или Пароль!', success: false, data });
              }).catch((error) => res.send({
                message: 'Аккаунт не верифицирован!', success: false, error, data,
              }));
          })
          .catch((error) => res.send({ message: 'Произошла ошибка авторизации', success: false, error }));
      }
    } else {
      res.send({ message: 'Попробуйте позже', success: false });
    }
  });
};
