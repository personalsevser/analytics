const Sequelize = require('sequelize');
const config = require('../db');

const db = new Sequelize(
  config.db.database,
  config.db.username,
  config.db.password,
  {
    host: config.db.host,
    dialect: config.db.dialect,
    logging: false,
    define: {
      timestamps: false,
    },
  },
);

module.exports = db;
