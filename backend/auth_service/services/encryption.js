const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const encrypt = {
  isValidPassword(userPassword, hashPassword) {
    return bcrypt.compareSync(userPassword, hashPassword);
  },
  generateHash(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
  },
  generateToken() {
    return crypto.randomBytes(20).toString('hex');
  },
};

module.exports = encrypt;
