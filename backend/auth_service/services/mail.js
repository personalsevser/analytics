const nodemailer = require('nodemailer');

module.exports = function (to, subject, text) {
  const transporter = nodemailer.createTransport({
    host: 'smtp.yandex.ru',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: 'info@test.wb-stat.ru', // generated ethereal user
      pass: 'wbstatpassword', // generated ethereal password
    },
  });

  return transporter.sendMail({
    from: 'info@test.wb-stat.ru', // sender address
    to, // list of receivers
    subject, // Subject line
    text, // plain text body
    // html: "<b>Hello world?</b>" // html body
  });
};
