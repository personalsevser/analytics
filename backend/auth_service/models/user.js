const Sequelize = require('sequelize');
const db = require('../provider');

const user = db.define('wb_users', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  login: {
    type: Sequelize.STRING(25),
    unique: true,
  },
  email: {
    type: Sequelize.STRING(45),
    allowNull: false,
    unique: true,
  },
  password: {
    type: Sequelize.STRING(255),
    allowNull: false,
  },
  verificationToken: {
    type: Sequelize.STRING(255),
  },
  resetPasswordToken: {
    type: Sequelize.STRING(255),
  },
  resetPasswordExpires: {
    type: Sequelize.DATE,
  },
  refreshToken: {
    type: Sequelize.STRING(255),
  },
});

module.exports = {
  db,
  user,
};
