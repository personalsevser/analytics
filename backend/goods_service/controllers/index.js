const Sequelize = require('sequelize');
const xslx = require('../services/xslxToData');
const dataToXSLX = require('../services/dataToXSLX');
const db = require('../models/good');
const tokenHandler = require('../services/token');
const paginate = require('../services/paginate');

const { goods_wb_fields, goods_wb } = require('../models/goods_wb');
const { goods_ozon_fields, goods_ozon } = require('../models/goods_ozon');

module.exports = function builder(app) {
  app.post('/filterOzon', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        const where = {
          owner_id: userInfo.id,
        };
        goods_ozon.findAll({
          where,
        })
          .then((result) => {
            res.send({
              message: 'success',
              success: true,
              result,
            });
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.post('/filterWb', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        const where = {
          owner_id: userInfo.id,
        };
        goods_wb.findAll({
          where,
          attributes: ['id', 'id_wb', 'barcode', 'brand', 'type', 'color', 'size', 'id_supps'],
        })
          .then((result) => {
            res.send({
              message: 'success',
              success: true,
              result,
            });
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.post('/create', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        let typeObject = null;
        const data = {};

        if (goods_wb_fields.every((field) => Object.hasOwnProperty.call(req.body, field))) {
          typeObject = 'wb';
          goods_wb_fields.forEach((key) => {
            data[key] = req.body[key];
          });
        } else if (goods_ozon_fields.every((field) => Object.hasOwnProperty.call(req.body, field))) {
          typeObject = 'ozon';
          goods_ozon_fields.forEach((key) => {
            data[key] = req.body[key];
          });
        } else {
          res.send(400, {
            message: 'Неверный набор параметров',
            success: false,
          });
        }

        data.owner_id = userInfo.id;

        console.log(data);
        console.log(typeObject);

        if (typeObject === 'wb') {
          goods_wb.create(data)
            .then((entity) => {
              res.send({
                message: 'Запись успешно создана',
                success: true,
                result: entity,
              });
            })
            .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
        } else {
          goods_ozon.create(data)
            .then((entity) => {
              res.send({
                message: 'Запись успешно создана',
                success: true,
                result: entity,
              });
            })
            .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
        }
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.post('/update', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        if (!(Object.hasOwnProperty.call(req.body, 'name')
        && Object.hasOwnProperty.call(req.body, 'price')
        && Object.hasOwnProperty.call(req.body, 'description')
        && Object.hasOwnProperty.call(req.body, 'size'))) {
          res.send(400, {
            message: 'Неверный набор параметров',
            success: false,
          });
        }
        const data = {
          name: req.body.name,
          price: req.body.price,
          description: req.body.description,
          size: req.body.size,
        };

        const where = {
          id: {
            [Sequelize.Op.eq]: req.body.id,
          },
          owner_id: {
            [Sequelize.Op.eq]: userInfo.id,
          },
        };


        db.goods.update(data, {
          where,
        })
          .then(() => {
            res.send({
              message: 'Запись успешно обвновлена',
              success: true,
            });
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.get('/viewWb', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        if (!req.query.id) {
          res.send(400, {
            message: 'Неверный набор параметров',
            success: false,
          });
        }
        const where = {
          owner_id: {
            [Sequelize.Op.eq]: userInfo.id,
          },
          id: {
            [Sequelize.Op.eq]: req.query.id,
          },
        };

        goods_wb.findOne({
          where,
        })
          .then((result) => {
            res.send({
              message: 'success',
              success: true,
              result,
            });
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.get('/view', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        if (!req.query.id) {
          res.send(400, {
            message: 'Неверный набор параметров',
            success: false,
          });
        }
        const where = {
          owner_id: {
            [Sequelize.Op.eq]: userInfo.id,
          },
          id: {
            [Sequelize.Op.eq]: req.query.id,
          },
        };

        db.goods.findOne({
          where,
        })
          .then((result) => {
            res.send({
              message: 'success',
              success: true,
              result,
            });
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.post('/filter', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        const where = {
          userId: userInfo.id,
        };
        if (Object.hasOwnProperty.call(req.body, 'minPrice')) {
          where.price = {
            [Sequelize.Op.gte]: req.body.minPrice,
          };
        } else if (Object.hasOwnProperty.call(req.body, 'maxPrice')) {
          where.price = {
            [Sequelize.Op.lte]: req.body.maxPrice,
          };
        }
        if (Object.hasOwnProperty.call(req.body, 'minSize')) {
          where.size = {
            [Sequelize.Op.gte]: req.body.minSize,
          };
        } else if (Object.hasOwnProperty.call(req.body, 'maxSize')) {
          where.size = {
            [Sequelize.Op.lte]: req.body.maxSize,
          };
        }
        if (Object.hasOwnProperty.call(req.body, 'name')) {
          where.name = {
            [Sequelize.Op.substring]: req.body.name,
          };
        }
        db.goods.findAll(paginate({
          where,
        }, {
          page: req.body.page || 0,
          pageSize: req.body.pageSize > 10 ? req.body.pageSize : 10,
        }))
          .then((result) => {
            res.send({
              message: 'success',
              success: true,
              result,
            });
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.post('/exportXSLX', (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        const where = {
          owner_id: userInfo.id,
        };
        if (Object.hasOwnProperty.call(req.body, 'minPrice')) {
          where.price = {
            [Sequelize.Op.gte]: req.body.minPrice,
          };
        } else if (Object.hasOwnProperty.call(req.body, 'maxPrice')) {
          where.price = {
            [Sequelize.Op.lte]: req.body.maxPrice,
          };
        }
        if (Object.hasOwnProperty.call(req.body, 'minSize')) {
          where.size = {
            [Sequelize.Op.gte]: req.body.minSize,
          };
        } else if (Object.hasOwnProperty.call(req.body, 'maxSize')) {
          where.size = {
            [Sequelize.Op.lte]: req.body.maxSize,
          };
        }
        if (Object.hasOwnProperty.call(req.body, 'name')) {
          where.name = {
            [Sequelize.Op.substring]: req.body.name,
          };
        }
        db.goods.findAll({
          where,
        })
          .then((result) => {
            console.log(result);
            res.setHeader('Content-disposition', 'attachment; filename=Goods.xlsx');
            res.end(dataToXSLX(result), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            // res.download(dataToXSLX(result));
          })
          .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
  app.post('/importFromXSLX', async (req, res) => {
    tokenHandler(req.headers.authorization)
      .then((userInfo) => {
        if (!req.files && !req.files.file && !req.files.file.data) {
          return res.status(400).send({
            message: 'Файл не был прикреплен',
            success: false,
          });
        }
        const fields = xslx.getFields(req.files.file.data);
        const fieldsWb = xslx.getFieldsWb(req.files.file.data);
        if (fieldsWb
          .every(
            (headerItem) => goods_wb_fields.find((field) => field.label === headerItem),
          )
          && fieldsWb.length) {
          const workSheetsFromBuffer = xslx.exportFromXSLXWb(req.files.file.data, fieldsWb, goods_wb_fields);
          const data = workSheetsFromBuffer.map((item) => ({ owner_id: userInfo.id, ...item }));

          console.log(data);

          goods_wb
            .findAll({
              where: {
                id_wb: {
                  [Sequelize.Op.in]: data.map(({ id_wb }) => id_wb),
                },
                owner_id: {
                  [Sequelize.Op.eq]: userInfo.id,
                },
              },
            })
            .then((result) => {
              console.log(result);
              if (!result.length
                || result.every((good) => !data.find((newGood) => newGood.id_wb === good.id_wb
                  && newGood.color === good.color
                  && newGood.size === good.size))) {
                Promise
                  .all(data.map((item) => goods_wb.create(item)))
                  .then((result) => {
                    res.send({
                      message: 'Данные товары успешно импортированы',
                      success: true,
                      result,
                    });
                  })
                  .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
              } else {
                res.send({
                  message: 'Невозможно импортировать данные',
                  success: false,
                  error: {
                    message: `Товары "${result.map(({ barcode, size, color }) => `${barcode}, ${size}, ${color}`).join('", "')}" уже присутствуют в системе`,
                  },
                });
              }
            })
            .catch((error) => res.send(500, { message: 'Внутренняя ошибка сервера', success: false, error }));
        } else {
          return res.status(400).send({
            message: 'Формат файла не соответствует спецификации',
            success: false,
          });
        }
      })
      .catch((error) => {
        res.send(401, {
          message: 'Пользователь не авторизован',
          success: false,
          error,
        });
      });
  });
};
