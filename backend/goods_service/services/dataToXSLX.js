const xlsx = require('node-xlsx');

module.exports = function (data) {
  const fieldNames = ['name', 'barcode', 'price', 'description', 'size'];
  const formattedValues = data.map(item => [item['name'], item['barcode'], item['price'], item['description'], item['size']])
  const array = [fieldNames].concat(formattedValues);
  console.log(array);
  return xlsx.build([{name: 'Товары', data: array}]);
};
