const Sequelize = require('sequelize');
const db = require('../provider');

const goods = db.define('wb_goods', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  userId: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING(45),
  },
  barcode: {
    type: Sequelize.STRING(45),
    allowNull: false,
    unique: true,
  },
  price: {
    type: Sequelize.DECIMAL(15),
    allowNull: false,
  },
  description: {
    type: Sequelize.STRING(255),
  },
  size: {
    type: Sequelize.INTEGER,
  },
});

module.exports = {
  db,
  goods,
};
