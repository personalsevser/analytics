const Sequelize = require('sequelize');
const db = require('../provider');

const fields = {
  id: {
    type: Sequelize.INTEGER,
  },
  value: {
    type: Sequelize.INTEGER,
  },
  value_type: {
    type: Sequelize.INTEGER,
  },
  update_date: {
    type: Sequelize.DATE,
    allowNull: false,
    unique: true,
  },
};

const goods_prices = db.define('goods_prices', fields, { freezeTableName: true });

module.exports = {
  db,
  goods_prices,
  goods_prices_fields: fields,
};
