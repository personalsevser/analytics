const Sequelize = require('sequelize');
const db = require('../provider');

const fields = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  owner_id: {
    type: Sequelize.INTEGER,
  },
  brand: {
    type: Sequelize.STRING(45),
  },
  type: {
    type: Sequelize.STRING(45),
  },
  id_wb: {
    type: Sequelize.STRING(45),
  },
  chrt_id_niu: {
    type: Sequelize.STRING(45),
  },
  id_supps: {
    type: Sequelize.STRING(45),
  },
  id_supps_short_niu: {
    type: Sequelize.STRING(45),
  },
  color: {
    type: Sequelize.STRING(45),
  },
  size: {
    type: Sequelize.STRING(45),
  },
  barcode: {
    type: Sequelize.STRING(45),
  },
  sells_by: {
    type: Sequelize.STRING(5),
  },
};


const keyFields = Object.keys(fields);
keyFields.splice(0, 2);

const goods_wb_fields = [{
  fieldName: 'brand',
  label: 'Бренд',
}, {
  fieldName: 'type',
  label: 'Предмет',
}, {
  fieldName: 'id_wb',
  label: 'Номенклатура', //+
}, {
  fieldName: 'chrt_id_niu',
  label: 'Код размера (chrt_id)',
}, {
  fieldName: 'id_supps',
  label: 'Артикул поставщика',
}, {
  fieldName: 'id_supps_short_niu',
  label: 'Артикул ИМТ',
}, {
  fieldName: 'color',
  label: 'Артикул Цвета', //+
}, {
  fieldName: 'size',
  label: 'Размер', //+
}, {
  fieldName: 'barcode',
  label: 'Баркод',
}, {
  fieldName: 'price',
  label: 'Розничная цена, руб.',
}, {
  fieldName: 'sells_by',
  label: 'Комплектация',
}];

const goods_wb = db.define('goods_wb', fields, {
  freezeTableName: true,
  charset: 'utf8',
  collate: 'utf8_general_ci',
});

module.exports = {
  db,
  goods_wb,
  goods_wb_fields,
};
