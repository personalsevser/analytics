const Sequelize = require('sequelize');
const db = require('../provider');

const fields = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  owner_id: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  id_supps: {
    type: Sequelize.STRING(45),
  },
  ozon_prod_id: {
    type: Sequelize.STRING(45),
  },
  fbo_sku_id: {
    type: Sequelize.STRING(45),
  },
  fbs_sku_id: {
    type: Sequelize.STRING(45),
  },
  crossborder_sku: {
    type: Sequelize.STRING(45),
  },
  prod_name: {
    type: Sequelize.STRING(45),
  },
  barcode: {
    type: Sequelize.STRING(45),
  },
  prod_status: {
    type: Sequelize.STRING(45),
  },
  visible: {
    type: Sequelize.STRING(45),
  },
  creation_date: {
    type: Sequelize.DATE,
  },
  ozon_warehouse_available: {
    type: Sequelize.INTEGER,
  },
  reserved: {
    type: Sequelize.INTEGER,
  },
  supps_warehouse_availabrandble: {
    type: Sequelize.INTEGER,
  },
  recommended_price: {
    type: Sequelize.INTEGER,
  },
  recommended_price_link: {
    type: Sequelize.STRING(125),
  },
  brand: {
    type: Sequelize.STRING(45),
  },
};

const keyFields = Object.keys(fields);
keyFields.splice(0, 2);

const goods_ozon_fields = keyFields;

const goods_ozon = db.define('goods_ozon', fields, { freezeTableName: true });

module.exports = {
  db,
  goods_ozon,
  goods_ozon_fields,
};
