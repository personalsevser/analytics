const Sequelize = require('sequelize');
const db = require('../provider');

const fields = {
  id_price_type: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  price_type: {
    type: Sequelize.STRING(45),
  },
};

const prices_types = db.define('prices_types', fields, {freezeTableName:true});

module.exports = {
  db,
  prices_types,
  prices_types_fields: fields,
};
