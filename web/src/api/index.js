import axios from 'axios';

let url = '';

if (process.env.NODE_ENV === 'production') {
  // url = 'http://test.wb-stat.ru';
  url = 'http://185.251.91.246';
}
if (process.env.NODE_ENV === 'development') {
  url = 'http://localhost:4500';
}

const authPath = '/api/auth/';
const goodsPath = '/api/goods/';
const organizationsPath = '/api/organizations/';

export const signup = (params) => axios.post(`${url}${authPath}signup`, params);

export const signin = (params) => axios.post(`${url}${authPath}signin`, params);
export const getUserParams = (params) => axios.post(`${url}${authPath}getUserParams`, params);
export const refreshToken = (params) => axios.post(`${url}${authPath}refreshToken`, null, params);
export const forgotPassword = (params) => axios.post(`${url}${authPath}forgotPassword`, params);
export const resetPassword = (params) => axios.post(`${url}${authPath}resetPassword`, params);

export const checkToken = (params) => axios.post(`${url}${authPath}verificateToken`, params);

export const createGood = (params) => axios.post(`${url}${goodsPath}create`, params);
export const filterOzon = (params) => axios.post(`${url}${goodsPath}filterOzon`, params);
export const filterWb = (params) => axios.post(`${url}${goodsPath}filterWb`, params);
export const viewWb = (params) => axios.post(`${url}${goodsPath}viewWb`, params);
export const createOrganization = (params) => axios.post(`${url}${organizationsPath}create`, params);
export const filterOrganization = (params) => axios.post(`${url}${organizationsPath}filter`, params);

export const importFromXSLX = (params) => {
  const form = new FormData();
  form.append('file', params.file);
  return axios.post(`${url}${goodsPath}importFromXSLX`, form);
};
