import Vue from 'vue';
import axios from 'axios';
import BootstrapVue from 'bootstrap-vue';
import { VueReCaptcha } from 'vue-recaptcha-v3';

import App from './App';
import router from './router';
import store from './store';
import './styles/index.scss';

Vue.config.productionTip = false;
Vue.use(VueReCaptcha, { siteKey: '6LdyfMUUAAAAAIATxTOfqaqmz_a9TVj226OljOk_' });
Vue.use(BootstrapVue);

axios.defaults.withCredentials = true;
axios.interceptors.response
  .use((response) => {
    if (!response.data.success) {
      return Promise.reject(response);
    }
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      router.push({
        path: '/Account',
      });
    }
    return Promise.reject(error);
  });


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
