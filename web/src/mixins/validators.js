export default {
  methods: {
    validatePassword(pass) {
      // eslint-disable-next-line
      return /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]{8,})$/.test(pass);
    },
    validateEmail(mail) {
      // eslint-disable-next-line
      return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail);
    },
  },
};
