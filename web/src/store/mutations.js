import axios from 'axios';

export const SHOW_SPINNER = (state) => {
  state.countRequest += 1;
};

export const HIDE_SPINNER = (state) => {
  state.countRequest -= 1;
};

export const SET_SIGNIN_MESSAGE = (state, { resp }) => {
  state.auth.signinMesage = resp.message;
};

export const CLEAR_SIGNIN_MESSAGE = (state) => {
  state.auth.signinMesage = null;
};

export const CLEAR_TOAST = (state) => {
  state.toast = null;
};

export const SET_DATA_TO_TOAST = (state, toast) => {
  state.toast = toast;
};


export const SET_AUTH_PARAMETERS = (state, { resp }) => {
  state.auth.accessToken = resp.accessToken;
  state.auth.refreshToken = resp.refreshToken;
  axios.defaults.headers.common.Authorization = `Bearer ${resp.accessToken}`;
};

export const SET_FOUND_OZON_GOODS = (state, resp) => {
  state.goods.ozon.filtered = resp;
};

export const CLEAR_FOUND_OZON_GOODS = (state) => {
  state.goods.ozon.filtered = [];
};


export const SET_FOUND_WB_GOODS = (state, resp) => {
  state.goods.wb.filtered = resp;
};

export const CLEAR_FOUND_WB_GOODS = (state) => {
  state.goods.wb.filtered = [];
};
