export default {
  countRequest: 0,
  toast: null,
  auth: {
    accessToken: null,
    refreshToken: null,
    expiresIn: null,
    signinMesage: null,
  },
  goods: {
    ozon: {
      filtered: [],
    },
    wb: {
      filtered: [],
    },
  },
};
