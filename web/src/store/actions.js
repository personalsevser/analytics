import * as api from '../api';
import router from '../router';

export const signup = ({ commit, dispatch }, params) => {
  dispatch('showSpinner');
  api.signup(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Register?send=true',
        });
      }
    })
    .catch(({ response }) => {
      commit('SET_SIGNIN_MESSAGE', response.data);
    })
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const signin = ({ commit, dispatch }, params) => {
  dispatch('showSpinner');
  commit('CLEAR_SIGNIN_MESSAGE');
  api.signin(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Profile',
        });
        commit('SET_AUTH_PARAMETERS', { resp: resp.data });
      }
    })
    .catch(({ response }) => {
      if (!response.data.success) {
        commit('SET_SIGNIN_MESSAGE', { resp: response.data });
      }
    })
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const forgotPassword = ({ dispatch }, params) => {
  dispatch('showSpinner');
  api.forgotPassword(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Forgot?send=true',
        });
      }
    })
    .catch((error) => { console.log(error); })
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const resetPassword = ({ commit, dispatch }, params) => {
  dispatch('showSpinner');
  api.resetPassword(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Account',
        });
        commit('SET_DATA_TO_TOAST', {
          label: ''.concat(resp.data.message),
          title: 'Смена пароля',
          variant: 'success',
        });
      }
    })
    .catch((error) => {
      commit('SET_DATA_TO_TOAST', {
        label: ''.concat(error.response.data.message),
        title: 'Смена пароля',
        variant: 'warning',
      });
    })
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const checkToken = ({ dispatch, commit }, params) => {
  dispatch('showSpinner');
  api.checkToken(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Account',
        });
        commit('SET_DATA_TO_TOAST', {
          label: ''.concat(resp.data.message),
          title: '',
          variant: 'success',
        });
      }
    })
    .catch((error) => { console.log(error); })
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const getUserParams = ({ dispatch }, params) => {
  dispatch('showSpinner');
  api.getUserParams(params)
    .then(() => {})
    .catch(() => {})
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const refreshToken = ({ commit, dispatch }, params) => {
  dispatch('showSpinner');
  api.refreshToken(params)
    .then((resp) => {
      if (resp.data.success) {
        commit('SET_AUTH_PARAMETERS', { resp: resp.data });
      }
    })
    .catch(() => {})
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const showSpinner = ({ commit }) => {
  commit('SHOW_SPINNER');
};

export const hideSpinner = ({ commit }) => {
  commit('HIDE_SPINNER');
};


export const clearSigninMessage = ({ commit }) => {
  commit('CLEAR_SIGNIN_MESSAGE');
};

export const clearToast = ({ commit }) => {
  commit('CLEAR_TOAST');
};

export const createOzonGood = ({ dispatch }, params) => {
  dispatch('showSpinner');
  api.createGood(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Goods/Ozon/Filter',
        });
      }
    })
    .catch(() => {})
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const createOrganization = ({ dispatch }, params) => {
  dispatch('showSpinner');
  api.createOrganization(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Profile/Organizations',
        });
      }
    })
    .catch(() => {})
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const filterOrganization = ({ dispatch }, params) => {
  dispatch('showSpinner');
  api.filterOrganization(params)
    .then((resp) => {
      if (resp.data.success) {
        router.push({
          path: '/Profile/Organizations',
        });
      }
    })
    .catch(() => {})
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const filterOzon = ({ commit, dispatch }, params) => {
  dispatch('showSpinner');
  api.filterOzon(params)
    .then((resp) => {
      if (resp.data.success) {
        commit('SET_FOUND_OZON_GOODS', resp.data.result);
      }
    })
    .catch(() => {
      commit('CLEAR_FOUND_OZON_GOODS');
    })
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const filterWb = ({ commit, dispatch }, params) => {
  dispatch('showSpinner');
  api.filterWb(params)
    .then((resp) => {
      if (resp.data.success) {
        commit('SET_FOUND_WB_GOODS', resp.data.result);
      }
    })
    .catch(() => {
      commit('CLEAR_FOUND_WB_GOODS');
    })
    .finally(() => {
      dispatch('hideSpinner');
    });
};

export const importFromXSLX = ({ dispatch }, params) => {
  dispatch('showSpinner');
  api.importFromXSLX(params)
    .then((resp) => console.log(resp))
    .catch((error) => console.error(error))
    .finally(() => {
      dispatch('hideSpinner');
    });
};
