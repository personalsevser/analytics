/* eslint-disable import/prefer-default-export */
export const getIsShowSpinner = state => Boolean(state.countRequest);
export const getAccessToken = state => state.auth.accessToken;
export const getRefreshToken = state => state.auth.refreshToken;
export const getSigninMesage = state => state.auth.signinMesage;
export const getToast = state => state.toast;

export const getOzonFiltered = state => state.goods.ozon.filtered;
export const getWbFiltered = state => state.goods.wb.filtered;
