import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/:component',
    component: () => import('../views/MainComponent'),
    children: [{
      path: '/Account',
      name: 'Login',
      component: () => import('../views/Account'),
    }, {
      path: '/Goods/:section',
      redirect: '/Goods/:section/Filter',
    }, {
      path: '/Goods/:section/:action',
      name: 'GoodsSection',
      props: true,
      component: () => import('../views/Goods'),
    }, {
      path: '/Goods/:section/:action/:id',
      name: 'GoodsSection',
      props: true,
      component: () => import('../views/Goods'),
    }, {
      path: '/Goods',
      name: 'Goods',
      component: () => import('../views/Goods'),
    }, {
      path: '/Account/:token',
      name: 'VerificationAccount',
      props: true,
      component: () => import('../views/Account'),
    }, {
      path: '/Forgot',
      name: 'Forgot',
      props: true,
      component: () => import('../views/Forgot'),
    }, {
      path: '/Reset',
      name: 'Reset',
      component: () => import('../views/Reset'),
    }, {
      path: '/Reset/:token',
      name: 'ResetPassword',
      props: true,
      component: () => import('../views/Reset'),
    }, {
      path: '/About',
      name: 'AboutView',
      props: true,
      component: () => import('../views/About'),
    }, {
      path: '/Analytics',
      name: 'AnalyticsView',
      props: true,
      component: () => import('../views/Analytics'),
    }, {
      path: '/Contacts',
      name: 'ContactsView',
      props: true,
      component: () => import('../views/Contacts'),
    }, {
      path: '/Documents',
      name: 'DocumentsView',
      props: true,
      component: () => import('../views/Documents'),
    }, {
      path: '/Home',
      name: 'HomeView',
      props: true,
      component: () => import('../views/Home'),
    }, {
      path: '/Profile',
      name: 'ProfileView',
      props: true,
      component: () => import('../views/Profile'),
    }, {
      path: '/Profile/*',
      name: 'ProfileSubmodule',
      props: true,
      component: () => import('../views/Profile'),
    }, {
      path: '/Register',
      name: 'RegisterView',
      props: true,
      component: () => import('../views/Register'),
    }],
  }, {
    path: '/',
    redirect: '/Home',
  }, {
    path: '',
    redirect: '/Home',
  }, {
    path: '/*',
    redirect: '/Home',
  }],
});
